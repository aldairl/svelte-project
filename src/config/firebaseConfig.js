import { initializeApp } from "firebase/app";
import { getFirestore } from 'firebase/firestore'

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDL8VmVL3LtYavMeBa_bd3rIbZtJvSV4jo",
  authDomain: "save-data-svelte.firebaseapp.com",
  projectId: "save-data-svelte",
  storageBucket: "save-data-svelte.appspot.com",
  messagingSenderId: "935368466412",
  appId: "1:935368466412:web:f579dd37552348c983aac9"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);